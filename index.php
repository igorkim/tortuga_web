<?
    
    include("php/func.php");
	include("php/sniffer.php");
	
	if (!($_GET["section"] > 0)) $_GET["section"] = 1;
	
	if (($_GET["section"] > 0) && (!($_GET["page"] > 0)))
		include("php/show_section.php");
	elseif ($_GET["page"] > 0){
		if ($_GET["page"] == 2)
			include("php/mail_form.php");
		elseif ($_GET["page"] == 3)
			include("php/check_form.php");
		else{
			include("php/show_page.php");
		}
	}
    
    /*Обработка событий do*/
    if ($_GET["do"] == "add_to_cart") /*Добавление в корзину*/
    {
        add_to_cart($_GET["id"]);
    }
    if ($_GET["do"] == "minus_from_cart") /*Удаление из корзины*/
    {
        minus_from_cart($_GET["id"]);
    }
    if ($_GET["do"] == "delete_from_cart") /*Удаление всех элементов типа в корзину*/
    {
        delete_from_cart($_GET["id"]);
    }
    if ($_GET["do"] == "order") /*Отображение формы заказа*/
    {
        include("php/order_form.php");
    }
    if ($_GET["do"] == "mail") /*Отображение формы заказа*/
    {
        include("php/mail_form.php");
    }
    if ($_GET["do"] == "check") /*Отображение формы заказа*/
    {
        include("php/check_form.php");
    }
    if ($_GET["do"] == "ref_check") /*Отображение формы заказа*/
    {
        include("php/check.php");
    }
    
    /*Загрузка категорий меню и корзины*/
    $sections 	= show_sections(); /* Функция из  func.php*/
	$pages		= show_pages(); /* Функция из  func.php*/
    $cart        = show_cart_info(); /* Функция из func.php */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="ru">
<meta name="description" content="Доставка еды, шашлык и барбекю, г. Южно-Сахалинск">
<meta name="keywords" content="шашлык, барбекю, гриль, салаты, фри, восточные салаты, европейские салаты, интернет-магазин, доставка еды, южно-сахалинск, сахалин">
<title>TORTUGA. гриль & барбекю. г. Южно-Сахалинск, Сахалин, доставка еды</title>
<link href="main.css" rel="stylesheet" type="text/css" /><!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* это отрицательное поле в 1 пиксел можно поместить в любом столбце данного макета с таким же корректирующим эффектом. */
ul.nav a { zoom: 1; }  /* свойство масштабирования предоставляет IE триггер hasLayout, необходимый для удаления лишнего пустого пространства между ссылками */
</style>
<![endif]-->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
</head>

<body>
<div class="header"><!-- end .header --></div>

<div class="ContHead">
	<div class="ContHeadL"></div>
	<div class="ContHeadC"></div>
	<div class="ContHeadR"></div>
</div>


<div class="container">


	<div class="sidebar1">
		<ul class="nav">
      		<? echo $sections; ?>
		</ul>
		
		<ul class="nav">
			<? echo $pages; ?>
		</ul>
	
		<p class="BasketTxt">Мой заказ <div id="cart_load" style="display:none;"><img src="images/loading (2).gif" /></div></p>
		<div id="cart">
		<? echo $cart; ?>
		</div>
		<p class="BasketStart"><a onclick="show_order_form(); return false" href="?do=order">Оформить заказ</a></p>
	<!-- end .sidebar1 -->
	</div>
	
	<div id="loading" class="contentLoad">
	<img src="images/loading (1).gif" />
 	<!-- end .contentLoad -->
	</div>
	
<div id="content">
	<? echo $body; ?>
</div>
	
  <div class="footer">
  <!-- end .footer -->
  </div>
    
  <!-- end .container --></div>
  
   <div class="footerP">
   Телефон доставки: 612-000
  <!-- end .footer -->
  </div>
  	

  
</body>
</html>
