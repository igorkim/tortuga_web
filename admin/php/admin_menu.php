<? session_start();
    
    header('Content-Type: text/html; charset=utf-8');
    
    include("admin_func.php");
    
    if ($_GET["do"] == "show_cats"){
        $body = show_cats();
    }
	
    if ($_GET["do"] == "show_menu"){
        $body = show_menu($_GET["section"]);
    }
    if ($_GET["do"] == "change_stick"){
        $body = change_stick_menu($_GET["id"], $_GET["meth"]);
    }
    if ($_GET["do"] == "change_stick_section"){
        $body = change_stick_section($_GET["id"], $_GET["meth"]);
    }
    if ($_GET["ajax"] == "1"){
     echo $body;
    }
    
?>
