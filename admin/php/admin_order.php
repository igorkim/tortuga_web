<? session_start();
    
    header('Content-Type: text/html; charset=utf-8');
	
    include("admin_func.php");
    
    if ($_GET["do"] == "show_orders"){
        $body = show_orders($_GET["status"]);
    }
	
    if ($_GET["do"] == "change_status"){
        $body = change_status($_GET["id"], $_GET["to"]);
    }
    if ($_GET["do"] == "view_order"){
        $body = view_order($_GET["id"]);
    }
    
    if ($_GET["ajax"] == "1"){
     echo $body;
    }
    
?>
