<? session_start();
	date_default_timezone_set('Asia/Vladivostok');
	if (is_file("../../php/mysql.php"))
   	 	include("../../php/mysql.php"); 
   	else include("../php/mysql.php"); 
    $sqlconn = mysql_connect ($sql_host, $sql_login, $sql_pass); 
    if (!$sqlconn) die('Cannot connect to MySQL base: ' .mysql_error());
    mysql_select_db ($sql_base) or die('Cannot connect to MySQL base: ' .mysql_error());
    
    mysql_query ("set character_set_client='utf8'"); 
    mysql_query ("set character_set_results='utf8'"); 
    mysql_query ("set collation_connection='utf8_general_ci'");
    
    header('Content-Type: text/html; charset=utf-8');
	
	
//***********Общие функции*****************//
	function getExtension($filename) { //Определяет расширение файла
   		return end(explode(".", $filename));
  	}
//********************************************************************//
//********************************************************************//
//********************************************************************//


	
//***********Общие для работы с заказами*****************//
    function status_txt($status){ //Переводит статус заказа из числового в строковый видs
        $status++;
        $req  = mysql_query("SELECT * FROM status WHERE status.index = '$status'");
        $row = mysql_fetch_array($req);
        $res  = $row["name"];
        return $res; 
    }
	
    function change_status($index, $status){ //Изменяет статус заказа, где $status - новый статус заказа в числовом виде,  $index - id заказа
		$st = $status;
		if ($st == 0) $st = "";
		$time   = time();
        $date   = date("Y-m-d", $time);
        $time   = date("H:i:s", $time);		
		$dt = "date".$st;
		$tm = "time".$st;
		
		if ($st != "")
			$req = mysql_query("UPDATE orders SET status = '$status', $dt = '$date', $tm = '$time' WHERE orders.index = '$index'"); 
		else $req = mysql_query("UPDATE orders SET status = '$status' WHERE orders.index = '$index'");
    }
    
    function view_order($index){ //Возвращает содержимое заказа
        $req = mysql_query("SELECT * FROM orders WHERE orders.index = '$index'"); 
        $row = mysql_fetch_array($req);
        $t   = $row["order"];
        $res = "<h1>Заказ #".$index."</h1>";
        $t   = substr($t, 0, strlen($t)-1);
        $c   = substr_count($t, "|")+1;
        $t   = explode("|", $t);
        
        $res .= "<table class=\"sample\">";
        $res .= "<TR><TH>ID</TH><TH>Название</TH><TH>Цена</TH><TH>Количество</TH><TH>Стоимость</TH></TR>";
        $sum = 0;
        for ($i=0; $i<$c; $i++){
            $res .= "<TR>";
            $o  = explode("-", $t[$i]);
            $id = $o[0];
            $count = $o[1];
            $req  = mysql_query("SELECT * FROM products WHERE products.index = '$id'");
            $row  = mysql_fetch_array($req);
            $res .= "<TD>".$row["index"]."</TD>";
            $res .= "<TD>".$row["name"]."</TD>";
            $res .= "<TD>".$row["cost"]."</TD>";
            $res .= "<TD>".$count."</TD>";
            $total = $count*$row["cost"];
            $res .= "<TD>".$total."</TD>";
            $name = $row["name"];
            $cost = $row["cost"];
            $res .= "</TR>";
            $sum = $sum + $total;
        }
        $res .= "<TR><TH>Общая сумма</TH><TD>".$sum."</TD></TR>";
        $res .= "</TABLE>";
        return $res;
    }
	
	function status_list($id, $status){ //Возвращает список всех возможных состояний заказа, где $status - текущее состояние заказа (будет выбрано),  $id - номер текущего заказа
        $req  = mysql_query("SELECT * FROM status");
		$cnt = mysql_num_rows($req);
      
        if ($cnt > 0){
			$res  = "<select onchange=\"change_status(".$id.", ".$status.", this); return false\" name=\"status\" id=\"status\">";
			
            for ($i=0; $i<$cnt; $i++){ 
				$row = mysql_fetch_array($req);
				if ($status == $i)
					$res .= "<option value=\"".$row["name"]."\" selected>".$row["name"]."</option>"; 
				else
					$res .= "<option value=\"".$row["name"]."\" >".$row["name"]."</option>"; 
			}
			$res .= "</select>";
		}
        return $res;
	}
    
    function show_orders($status){ //Возвращает все заказа состояния $status
        $req = mysql_query("SELECT * FROM orders WHERE status = '$status'");
        $cnt = mysql_num_rows($req);
		$res = "<h2>".status_txt($status)."</h2>";

        
        if ($cnt > 0){
       	 	$res .= "<table class=\"sample\">";
        	$res .= "<TR><TH>ID</TH><TH>Дата</TH><TH>Время</TH><TH>Name</TH><TH>Адрес</TH><TH>Ст-х пр-в</TH><TH>Телефон</TH><TH>Примечание</TH><TH>Статус</TH></TR>";
            for ($i=0; $i<$cnt; $i++){ 
                $row = mysql_fetch_array($req);
				$sidm = explode(":", $row["time"]);
				$sid  = ($sidm[1]+28).$row["index"].($sidm[0]+39);
                $res .= "<TR><TD>".$sid."</TD><TD>".$row["date"]."</TD><TD>".$row["time"]."</TD><TD>".$row["name"]."</TD><TD>".$row["adress"]."</TD><TD>".$row["scount"]."</TD><TD>".$row["phone"]."</TD><TD>".$row["other"]."</TD><TD><div id=\"stat\">".status_list($row["index"], $row["status"])."</div></TD>";
                    
                $res .= "<TD><a onclick=\"view_order(".$row["index"]."); return false\" href=\"\">Посмотреть</a></TD>";
                $res .= "</TR>";
            }
			 $res .= "</TABLE>";
        }
		else $res .= "Нет заказов в этой категории";
       
        return $res;
    }
//********************************************************************//
//********************************************************************//
//********************************************************************//



	
//********************************************************************//	
//********************************************************************//	
//********************************************************************//
//***********Общие для работы со статистикой*****************//
	function view_statistic_files(){
		$dir 	= "../../statistic/";
		if (is_dir($dir)) {
   			$files = scandir($dir);
			$cnt   = count($files);
			$res 	  = "<ul>";
			for ($i=0; $i<$cnt; $i++){
				if (getExtension($files[$i]) == "csv"){
					$res .= "<li><a onclick=\"view_statistic($i, 0); return false\" href=\"?do=statistic&file=$files[$i]\">".$files[$i]."</a></li>";
					$_SESSION["files"][$i] = $files[$i];
				}
			}
			$res .= "</ul>";
		}	
		return $res;
	}
	
	function view_statistic_table($file){
		$mobiles= "Android|iPhone|Symbian OS|PalmOS|Mobile Java";
		$ips   	= "";
		$mobi   = 0;
		$bot	= 0;
		$bot_uniq	= 0;
		$uniq 	= 0;
		$hits   = 0;
		$file   = $_SESSION["files"][$file];
		$res 	= "<h2>".$file."</h2>";
		$file 	= "../../statistic/".$file;

		if (is_file($file)){
		    $res .= "<table class=\"sample\">";
       		$res .= "<TR><TH>Дата</TH><TH>Время</TH><TH>IP</TH><TH>Браузер</TH><TH>ОС</TH><TH>Активность</TH></TR>";
			$file_array = file($file);
			$num_str 	= count($file_array); 

			for ($i=($num_str-1); $i>0; $i--){ 
				$line = $file_array[$i];
	
				$line = explode(";", $line);
				
				$time = $line[6];

				if (ereg("^[0-9]+$", $time)) $ok = "1";


				if (($time != "") && ($ok == "1")){
					//echo "1";
					$ntime  = time();
					$ntime 	= $ntime - $time;
						if (($ntime / 86400) >= 1)	$ntime = round($ntime / 86400)." дней назад";
					elseif (($ntime / 3600) >= 1) 	$ntime = round($ntime / 3600)." час назад";
					elseif (($ntime / 60) >= 1)	 	$ntime = round($ntime / 60)." минут назад";
					else $ntime = $ntime." секунд назад";
					
					$date 	= date("Y-m-d", $time);
					$time   = date("H:i:s", $time);
					
					$ok    = "";
				}
				else{
					$date 		= $line[0];
					$time 		= $line[1];
					$ntime		= "";
				}
				
				if (stristr($mobiles, $line[4])) $mobi++;
				if (stristr($line[4], "Bot")) $bot++;
				if (!(stristr($ips, $line[2]))) {
					$ips_list_order[$uniq] = $line[2];
					$uniq++;
					if (stristr($line[4], "Bot")) $bot_uniq++;
					$ips_list[$line[2]][0] = 1;
					$ips_list[$line[2]][4] = $date;
					$ips_list[$line[2]][5] = $time;
					$ips_list[$line[2]][2] = $line[3];
					$ips_list[$line[2]][3] = $line[4];
					$ips_list[$line[2]][1] = $ntime." ".$line[5];
					
					$ips_list[$line[2]][6]  = "<TR>";
					$ips_list[$line[2]][6] .= "<td>".$date."</td>";
					$ips_list[$line[2]][6] .= "<td>".$time."</td>";
					$ips_list[$line[2]][6] .= "<td>".$line[2]."</td>";
					$ips_list[$line[2]][6] .= "<td>".$line[3]."</td>";
					$ips_list[$line[2]][6] .= "<td>";
					if (stristr($mobiles, $line[4]))
						$ips_list[$line[2]][6] .= "<img src=\"../images/mobi.gif\">";
					$ips_list[$line[2]][6] .= $line[4]."</td>";
					$ips_list[$line[2]][6] .= "<td>".$line[5]."</td>";
					$ips_list[$line[2]][6] .= "</TR>";
					$ips 				   .= "|".$line[2];
				}
				else{
					$ips_list[$line[2]][0] 	= $ips_list[$line[2]][0] + 1;

					$ips_list[$line[2]][6] .= "<TR>";
					$ips_list[$line[2]][6] .= "<td>".$date."</td>";
					$ips_list[$line[2]][6] .= "<td>".$time."</td>";
					$ips_list[$line[2]][6] .= "<td>".$line[2]."</td>";
					$ips_list[$line[2]][6] .= "<td>".$line[3]."</td>";
					
					$ips_list[$line[2]][6] .= "<td>";
					if (stristr($mobiles, $line[4]))
						$ips_list[$line[2]][6] .= "<img src=\"../images/mobi.gif\">";
					$ips_list[$line[2]][6] .= $line[4]."</td>";
					
					$ips_list[$line[2]][6] .= "<td>".$line[5]."</td>";
					

					
					
					$ips_list[$line[2]][6] .= "</TR>";
				}
			
            	
				$hits++;	
			}
			
			for ($i=0; $i<$uniq; $i++){ 
				$ip = $ips_list_order[$i];
				$res .= "<script>";
			    $res .= "            function view_statistic_ip(id){
            	var 
            		ob=document.getElementById('more_'+id); 
            	if (ob.style.display == 'none')
            		ob.style.display='block';
            	else
            		ob.style.display='none';
            }";
           		$res .= "</script>";
           		
          	  	$res .= "<TR>";
            	$res .= "<td><b>".$ips_list[$ip][4]."</b></td>";
            	$res .= "<TD><b>".$ips_list[$ip][5]."</b></TD>";
            	$res .= "<TD><b><a target=\"_blank\" href=\"http://ip-whois.net/ip_geo.php?ip=".$ip."\">".$ip."</b></TD>";
            	$res .= "<TD><b>".$ips_list[$ip][2]."</b></TD>";
            	$res .= "<TD><b>".$ips_list[$ip][3]."</b></TD>";
            	$res .= "<TD><b><a onclick=\"view_statistic_ip($i); return false\" href=\"\">".$ips_list[$ip][1]." (".$ips_list[$ip][0]." действий)</a></b></TD>";
            	$res .= "</TR>";
            	$res .= "<TR><td colspan=6>";
            	$res .= "<table class=\"sample\" style=\"display:none\" id=\"more_$i\">".$ips_list[$ip][6]."</table>";	
            	$res .= "</td></TR>";	
			} 
			
			$res .= "</TABLE>";
			$res .= "Уникальных посещение: ".$uniq." (Без учета роботов: ".($uniq-$bot_uniq).")<br/>";
			$res .= "Всего посещений: ".$hits." (Без учета роботов: ".($hits-$bot).")<br/>";
			$res .= "С мобильных устройств: ".$mobi;
		}
		else $res .= "<h2>Файл не найден</h2>";
		return $res;
	}
//********************************************************************//
//********************************************************************//
//********************************************************************//
//********************************************************************//
  

//********************************************************************//	
//********************************************************************//	
//********************************************************************//
//***********Общие для работы со страницами*****************//	
    function show_pages(){
        $req = mysql_query("SELECT * FROM pages ORDER BY stick ASC");
        $cnt = mysql_num_rows($req);
		$res = "<h2>Страницы</h2>";
		if ($cnt > 0){
			$res .= "<table class=\"sample\">";	
			$res .= "<TR><TH>ID</TH><TH>Название</TH><TH>Редактировать</TH><TH>Просмотреть</TH><TH>Положение</TH></TR>";
			for ($i=0; $i<$cnt; $i++){ 
				 $row = mysql_fetch_array($req);
				 $res .= "<TR><TD>".$row["index"]."</TD><TD>".$row["name"]."</TD><TD><a onclick=\"edit_page(".$row["index"]."); return false\" href=\"\">Редактировать</a></TD><TD><a target=\"_blank\" href=\"".$web_host."/?page=".$row["index"]."\">Просмотр</a></TD><TD><input id=\"meth".$row["index"]."\" type=\"text\" value=\"".$row["stick"]."\"><a onclick=\"change_stick_page(".$row["index"]."); return false\" href=\"\">Сохранить</a></TD></TR>";
			}
			$res .= "<TR><TD><a onclick=\"show_pages(); return false\" href=\"\">Обновить</a></TD></TR>";
			$res .= "</TABLE>";
		}
		else	$res .= "<br/>Не найдено страниц";
		return $res;
	}
	
    function edit_page($id){
        $req 	= mysql_query("SELECT * FROM pages WHERE pages.index = '$id'");
		$row 	= mysql_fetch_array($req);
		$res 		= "<h2>".$row["name"]."</h2>";
		$res 		.= "<div align=\"center\"><form method=\"post\"  onsubmit=\"save_page(this); return false\"  action=\"\">";
		$res 		.= "<input type=\"text\" name=\"name\" id=\"name\" size=\"27\" value=\"".$row["name"]."\"><br/>";
		$res 		.= "<input type=\"text\" hidden=\"true\" name=\"id\" id=\"id\" size=\"27\" value=\"".$row["index"]."\"><br/>";
       	$res	 	.= "<TEXTAREA rows=30 cols=100 name=\"text\" id=\"text\" size=\"27\" value=\"\">".$row["text"]."</TEXTAREA><br/>";
		$res	 	.= "<input type=\"submit\" value=\"Сохранить\">";
        $res		.= "</form></div>";
		return $res;
	}
	
	function change_stick_page($id, $do){
		$req = mysql_query("UPDATE pages SET stick = $do WHERE pages.index = '$id'");
	}
	

    function save_page($id, $text, $name){
		if ($id == "") $res = "Неизвестная ошибка";
		elseif ($name == "") $res = "Ошибка! Проверьте заполненность поля \"Название\"";
		else{
        	$req	= mysql_query("UPDATE pages SET text='$text', name='$name' WHERE pages.index = '$id'");
			$res	= "Страница \"".$name."\" сохранена";
		}
		return $res;
	}   
//********************************************************************//
//********************************************************************//
//********************************************************************//
//********************************************************************//
  

//********************************************************************//	
//********************************************************************//	
//********************************************************************//
//***********Общие для работы с меню*****************//	
    function show_cats(){
        $req = mysql_query("SELECT * FROM sections ORDER BY stick ASC");
        $cnt = mysql_num_rows($req);
		$res = "<h2>Категории меню </h2>";
		if ($cnt > 0){
			$res .= "<table class=\"sample\">";	
			$res .= "<TR><TH>ID</TH><TH>Название</TH><TH>Положение</TH></TR>";
			for ($i=0; $i<$cnt; $i++){ 
				 $row = mysql_fetch_array($req);
				 $res .= "<TR><TD>".$row["index"]."</TD><TD><a onclick=\"show_menu(".$row["index"]."); return false\" href=\"\">".$row["name"]."</a></TD><TD><input id=\"meth".$row["index"]."\" type=\"text\" value=\"".$row["stick"]."\"><a onclick=\"change_stick_section(".$row["index"]."); return false\" href=\"\">Сохранить</a></TD></TR>";
			}
			$res .= "<TR><TD><a onclick=\"show_cats(); return false\" href=\"\">Обновить</a></TD></TR>";
			$res .= "</TABLE>";
		}
		else	$res .= "<br/>Не найдено страниц";
		return $res;
	}
	
    function show_menu($cat){
        $req = mysql_query("SELECT * FROM products WHERE section = $cat ORDER BY stick ASC");
        $cnt = mysql_num_rows($req);
		$res = "<h2>Меню</h2>";
		if ($cnt > 0){
			$res .= "<table class=\"sample\">";	
			$res .= "<TR><TH>ID</TH><TH>Название</TH><TH>Цена</TH><TH>Вес</TH><TH>Категория</TH><TH>Описание</TH><TH>Положение</TH></TR>";
			$sec = section_list($row["id"], $cat);
			for ($i=0; $i<$cnt; $i++){ 
				 $row = mysql_fetch_array($req);
				 $res .= "<TR><TD>".$row["index"]."</TD><TD>".$row["name"]."</TD><TD>".$row["cost"]."</TD><TD>".$row["weight"]."</TD><TD>".$sec."</TD><TD>".$row["desc"]."</TD><TD><input id=\"meth".$row["index"]."\" type=\"text\" value=\"".$row["stick"]."\"><a onclick=\"change_stick_menu(".$row["index"].", $cat); return false\" href=\"\">Сохранить</a></TD></TR>";
			}
			$res .= "<TR><TD><a onclick=\"show_menu(".$cat."); return false\" href=\"\">Обновить</a></TD></TR>";
			$res .= "</TABLE>";
		}
		else	$res .= "<br/>Не найдено страниц";
		return $res;
	} 
	
	 function change_stick_menu($id, $do){
		$req = mysql_query("UPDATE products SET stick = $do WHERE products.index = '$id'");
	}
	
	 function change_stick_section($id, $do){
		$req = mysql_query("UPDATE sections SET stick = $do WHERE sections.index = '$id'");
	}
	
	function section_list($id, $section){ //Выводи список категорий меню в виде выпадающего списка
        $req  = mysql_query("SELECT * FROM sections ORDER BY stick ASC");
		$cnt = mysql_num_rows($req);
		$section;
      
        if ($cnt > 0){
			$res  = "<select onchange=\"change_section(".$id.", ".$section.", this); return false\" name=\"section\" id=\"section\">";
			
            for ($i=0; $i<$cnt; $i++){ 
				$row = mysql_fetch_array($req);
				if ($section == $row["index"])
					$res .= "<option value=\"".$row["name"]."\" selected>".$row["name"]."</option>"; 
				else
					$res .= "<option value=\"".$row["name"]."\" >".$row["name"]."</option>"; 
			}
			$res .= "</select>";
		}
        return $res;
	}
	
//********************************************************************//
//********************************************************************//
//********************************************************************//
//********************************************************************//
?>
