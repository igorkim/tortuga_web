<?
	date_default_timezone_set('Asia/Vladivostok');
	if ( stristr($_SERVER['HTTP_USER_AGENT'], 'Firefox') )    		$browser = 'Firefox';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Chrome') )		$browser = 'Chrome';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Safari') )		$browser = 'Safari';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') )	$browser = 'Opera Mini';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Opera') )		$browser = 'Opera';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Avant') )		$browser = 'Avant';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Camino') )		$browser = 'Camino';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Epiphany') )		$browser = 'Epiphany';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Flock') )		$browser = 'Flock';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'iCab') )			$browser = 'iCab';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'IceCat') )		$browser = 'IceCat';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'IceWeasel') )	$browser = 'IceWeasel';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Konqueror') )	$browser = 'Konqueror';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 4.0') )		$browser = 'IE4';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') ) 	$browser = 'IE6';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') ) 	$browser = 'IE7';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0') ) 	$browser = 'IE8';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Netscape') )		$browser = 'Netscape';
	else $browser = "Other";
	
	
	
			if ( stristr($_SERVER['HTTP_USER_AGENT'], 'iPhone') )		$OC = 'iPhone';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Windows NT 5.0') )   $OC = 'Windows 2000';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Windows NT 5.1') )   $OC = 'Windows XP';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Windows NT 5.2') )   $OC = 'Windows 2003 / XP x64';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.0') )   $OC = 'Windows Vista';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') )   $OC = 'Windows 7';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Windows CE') )  		$OC = 'Windows Mobile';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Win') )    			$OC = 'Windows';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'iPhone') )			$OC = 'iPhone';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'iPad') )				$OC = 'iPad';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Android') )			$OC = 'Android';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'J2ME/MIDP') )		$OC = 'Mobile Java';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'SymbOS') )			$OC = 'Symbian OS';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Symbian OS') )		$OC = "Symbian OS";
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'PalmOS') )			$OC = 'PalmOS';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Mac') )				$OC = 'Mac OS X';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'PPC') )				$OC = 'Mac OS X';
	elseif ( stristr($_SERVER['HTTP_USER_AGENT'], 'Linux') )			$OC = 'Linux';
	else $OC = "Other";


	if (stristr($_SERVER['PHP_SELF'], 'index')){
	
		$time  = time();
		$mktime = $time;
    	$date  = date("Y-m-d", $time);
		$date1 = date("Y.m.d", $time);	
   		$time  = date("H:i:s", $time);
		$ip	   = $_SERVER['REMOTE_ADDR'];
	
	
		$file = "../statistic/".$date1."_admin.csv";
		
		if (is_file($file))
			$res  = file_get_contents($file);
		else $res = "";
			
		if ($res != "") $res .= "\n";
		$res .= $date.";".$time.";".$ip.";".$browser.";".$OC.";admin;".$mktime.";";
		file_put_contents($file, $res);
	}
/*	
	function getExtension($filename) {
   		return end(explode(".", $filename));
  	}
	
	if ($_GET["do"] == "show"){
		$dir 	= "../statistic/";
		$server = $_SERVER['HTTP_HOST'];
		if (is_dir($dir)) {
   			$files = scandir($dir);
			$cnt = count($files);
			
			for ($i=0; $i<$cnt; $i++){
				if (getExtension($files[$i]) == "csv"){
					echo "<a href=\"http://".$server."/statistic/".$files[$i]."\">".$files[$i]."</a>&nbsp;";
					echo "<a href=\"?do=show&mode=analyse&file=".$files[$i]."\">Analyse</a><br/>";
				}
			}
			
		}
		if ($_GET["mode"] == "analyse"){
			$file = "../statistic/".$_GET["file"];
			
			if (is_file($file)){
				$fileH = fopen($file, "r");
				$i = 1;
				$checkip = "";
				$checkoc = "";
				$checkbr = "";
				$oci      = -1;
				$bri      = -1;
				while (!(feof($fileH))){
					$line  = fgets($fileH);
	
					$line  = explode(";", $line);
					$ip    = $line[2];
					$br    = $line[3];
					$oc    = $line[4];
					
					if (!(stristr($checkip, $ip))){
						$checkip .= $ip."|";
						$u_ip     = $u_ip + 1; 
					}
					if (!(stristr($checkoc, $oc))){
						$checkoc .= $oc."|";
						$u_oc     = $u_oc + 1; 
						$oci++;
						$ocns[$oci] = $oc;
					}	
					if (!(stristr($checkbr, $br))){
						$checkbr .= $br."|";
						$u_br     = $u_br + 1; 
						$bri++;
						$brns[$bri] = $br;
					}						
					
					$occ[$oc] = $occ[$oc] + 1;	
					$brc[$br] = $brc[$br] + 1;		
					
					$i++;
				}
				fclose($fileH);
			}
			echo "<br/>Uniq Users: ".$u_ip."<br/><br/>";
			echo "<br/><br/> Uniq Browsers: ".$u_br."<br/>";
			for ($i=0; $i<$u_br; $i++){
				$n = $brns[$i];
				echo $brns[$i].": ".$brc[$n]."<br/>";
			}
			echo "<br/><br/> Uniq OCs: ".$u_oc."<br/>";
			for ($i=0; $i<$u_oc; $i++){
				$n = $ocns[$i];
				echo $ocns[$i].": ".$occ[$n]."<br/>";
			}				
		}		
	}
*/
?>