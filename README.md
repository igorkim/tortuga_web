# tortuga_web
My first single-page Shop. Written in 2012 now available for public.
Fully based on JavaScript (JQuery).

### It is an online shop for delivery service with administrative functions###
* orders, statuses.
* products, sections.
* pages.

### Users-side page ###
* cart.
* sections.
* order.
* check order status.

### Installation ###
* DB dump in tortuga.sql file.
* DB settings in /php/mysql.php.
* Admin in /admin section (password in index.php and can be changed).

### Try ###
* [Sample for Users](http://sandbox.roundeasy.ru/tortuga)
* [Sample for Admin](http://sandbox.roundeasy.ru/tortuga/admin) Password: tortuga2014