<? session_start();
	date_default_timezone_set('Asia/Vladivostok');
    include("mysql.php");
    $sqlconn = mysql_connect ($sql_host, $sql_login, $sql_pass); 
    if (!$sqlconn) die('Cannot connect to MySQL base: ' .mysql_error());
    mysql_select_db ($sql_base) or die('Cannot connect to MySQL base: ' .mysql_error());
    
    mysql_query ("set character_set_client='utf8'"); 
    mysql_query ("set character_set_results='utf8'"); 
    mysql_query ("set collation_connection='utf8_general_ci'");
    
    header('Content-Type: text/html; charset=utf-8');
	
    function status_txt($status){
        $status++;
        $req  = mysql_query("SELECT * FROM status WHERE status.index = '$status'");
        $row = mysql_fetch_array($req);
        $res  = $row["name"];
        return $res;
    }
    
    function show_sections(){ //Функция возвращает категории из базы
        $ss = "";
        $req = mysql_query("SELECT * FROM sections WHERE stick > -1 ORDER BY stick ASC");
        $cnt = mysql_num_rows($req);
        if ($cnt > 0){
            for ($i=0; $i<$cnt; $i++){ 
                $row = mysql_fetch_array($req);
                $ss .= "<li><a onclick=\"show_section(".$row["index"]."); return false\" href=\"?section=".$row["index"]."\">".$row["name"]."</a></li>";
        	}
        }
        return $ss;
    }
	
    function show_pages(){ //Функция возвращает категории из базы
        $ss = "";
        $req = mysql_query("SELECT * FROM pages WHERE stick > -1 ORDER BY stick ASC");
        $cnt = mysql_num_rows($req);
        if ($cnt > 0){
            for ($i=0; $i<$cnt; $i++){ 
                $row = mysql_fetch_array($req);
				if ($row["index"] == 2)
					$ss .= "<li><a onclick=\"show_mail_form(); return false\" href=\"?page=".$row["index"]."\">".$row["name"]."</a></li>";
            	elseif ($row["index"] == 3)
					$ss .= "<li><a onclick=\"show_check_form(); return false\" href=\"?page=".$row["index"]."\">".$row["name"]."</a></li>";    
				else
					$ss .= "<li><a onclick=\"show_page(".$row["index"]."); return false\" href=\"?page=".$row["index"]."\">".$row["name"]."</a></li>";
            }
        }
        return $ss;
    }
    
    function show_cart_info(){ //Функция возвращает корзину
        $req  = mysql_query("SELECT * FROM products");       
        $cnt  = mysql_num_rows($req);
        $sum  = 0;
        $prs  = 0;
        $all  = 0;
        if ($cnt > 0){
            for ($i=0; $i<$cnt; $i++){
                $row    = mysql_fetch_array($req);
                $cost   = $row["cost"];
				$id      = $row["index"];
                $count = $_SESSION["cart"][$id];
                $all      = $all + $count;
                $sum   = $sum + $cost*$count;
                if ($count > 0)
                { 
                    $res  .= "<div class=\"MenuBlock\">";
                    $res  .= "<div class=\"MenuRigth\">".$row["name"]."</div>";
                    $res  .= "<div class=\"MenuLeft\">";
                    $res  .= $count;
                    $res  .= "&nbsp;<a title=\"Увеличить количество\"  onclick=\"plus_to_cart(".$row["index"]."); return false\" href=\"?do=add_to_cart&id=".$row["index"]."\"><img alt=\"+\" align=\"bottom\" src=\"images/plus.png\" /></a>&nbsp;";
                    $res  .= "<a title=\"Уменьшить количество\" onclick=\"minus_from_cart(".$row["index"]."); return false\" href=\"?do=minus_from_cart&id=".$row["index"]."\"><img alt=\"-\" align=\"bottom\" src=\"images/minus.png\" /></a>&nbsp;";
                    $res  .= "<a title=\"Удалить из корзины\" onclick=\"delete_from_cart(".$row["index"]."); return false\" href=\"?do=delete_from_cart&id=".$row["index"]."\"><img alt=\"х\" align=\"bottom\" src=\"images/del.png\" /></a>";
                    $res  .= "</div>"; /*Êîíåö áëîêà MenuBlock*/
                    $prs++;
                    
                    $res  .= "</div>"; /*Êîíåö áëîêà MenuBlock*/
                }
            }
        }
        $res  .= "<div class=\"MenuSumBlock\">";
        $res  .= "<div class=\"MenuSumRigth\">Выбрано блюд</div>";
        $res  .= "<div class=\"MenuSumLeft\">".$all."</div>";
        $res  .= "</div>"; /*Êîíåö áëîêà MenuSumBlock*/
        
        $res  .= "<div class=\"MenuSumBlock\">";
        $res  .= "<div class=\"MenuSumRigth\">Сумма заказа</div>";
        $res  .= "<div class=\"MenuSumLeft\">".$sum." руб</div>";
        $res  .= "</div>"; /*Êîíåö áëîêà MenuSumBlock*/
		$_SESSION["sum"] = $sum;
        
        return $res;
    }
    
    function null_cart(){ //Функция обнуляет корзину
        $req  = mysql_query("SELECT * FROM products");       
        $cnt  = mysql_num_rows($req);
        if ($cnt > 0){
            for ($i=0; $i<$cnt; $i++){
                $row   = mysql_fetch_array($req); 
                $id = $row["index"]; 
                $_SESSION["cart"][$id] = 0;
            }
        }    
    }
	
    function do_mail($name, $phone, $email, $text){
			$to     		= "tortuga-food@mail.ru";
			$subject 	= "Новое сообщение TORTUGA";
			$headers ='Content-type: text/html; charset=UTF-8' . "\r\n".
				 'From: '. $email. "\r\n" .
   				 'Reply-To:'. $email. "\r\n" .
   				 'X-Mailer: PHP/' . phpversion();
			
			$message = "Получено новое сообщение\n<br/><br/>";
			$message .= "Отправитель: ".$name."<br/>";
			$message .= "Номер телефона: ".$phone."<br/>";
			$message .= "E-mail: ".$email."<br/><br/>";
			$text			 = str_replace("\n", "<br/>", $text);
			$message .= "Текст сообщения: <br/>".$text."<br/><br/>";
			$message .= "--<br/>";
			$message .= "<a href=\"http://tortuga-food.com/\">TORTUGA. Гриль & Барбекю</a>\n";			
			
			mail($to, $subject, $message, $headers);
			
			$res = "<div class=\"contentI\">Ваше сообщение отправлено! Ожидайте ответа оператора на указанный Вами e-mail или звонка.</div>";
			return $res;
     }
	 
    function do_check($number){
			$id 	= substr($number, 2);
			$id	= substr($id, 0, -2);
			$req   = mysql_query("SELECT * FROM orders WHERE orders.index = $id"); 
			$cnt   = mysql_num_rows($req);
			
			$res  = "<div class=\"contentI\">";
			if ($cnt != 1)
				$res .= "Заказ номер ".$number." не найден!";
			else{
				$row   = mysql_fetch_array($req);
				$res .= "Номер заказа: ".$number."<br/>";
				 for ($i=0; $i<4; $i++){
				 	$st = $i;
					if ($st == 0) $st = "";
					$dt = "date".$st;
					$tm = "time".$st;
					
				 	if ($i <= $row["status"])
						$res .= $row[$dt]." ".$row[$tm]." заказ ".status_txt($i)."<br/>";
					else
						$res .= "<strike> заказ ".status_txt($i)."</strike><br/>";
				 } 
				 $res .= "<a href=\"?do=ref_check&id=".$number."\" onclick=\"refresh_check(".$number."); return false\">Обновить</a>";
			}
			$res .= "</div>";
			return $res;
     }

    function do_order($name, $phone, $adress, $scount, $other){ //Функция добавляет заказ в базу
        $time   = time();
        $date   = date("Y-m-d", $time);
        $time   = date("H:i:s", $time);
        
        $req   = mysql_query("SELECT * FROM products");       
        $cnt   = mysql_num_rows($req);
        $sum   = 0;
        $order = ""; 

        if ($cnt > 0){
            for ($i=0; $i<$cnt; $i++){
                $row   = mysql_fetch_array($req);
                $id    = $row["index"];
                $count = $_SESSION["cart"][$id];
                $cost  = $row["cost"];
                $sum   = $sum + $cost*$count;
                if ($count > 0){
                    $order .= $id."-".$count."|";
                }
            }
        }
		
     /*   if ($sum < 400){
			$res = "<div class=\"contentI\">";
            $res .= "Ваш заказ не принят! Сумма заказа должна быть не менее 400-х рублей!<br/>";
			$res .= "<a onclick=\"show_section(1); return false\" href=\"?section=1#content\">Вернуться к выбору товара</a>";
			$res .= "</div>";
		}
        else{ */
            $req = mysql_query("INSERT INTO orders VALUES (0, '$order', '$name', '$adress', '$phone', 0, '$date', '$time', '0', '0', '0', '0', '0', '0', '$scount', '$other')");
            $res1 = mysql_insert_id();
			
			$sidm = explode(":", $time);
			$sid  = ($sidm[1]+28).$res1.($sidm[0]+39);
			
			$res  = "<div class=\"contentI\">";
			$res .= "Ваш заказ отправлен в службу доставки.<br/><br/>В ближайшее время с Вами свяжется оператор службы доставки для уточнения деталей заказа.<br/>Благодарим за выбор нашего ресторана!<br/>Также вы можете сделать заказ блюд по телефону 612-000.<br/><br/>";
            $res .= "Номер Вашего заказа ".$sid.".<br/>";
			$res .= "</div>";
			
			$to     		 = 'tortuga-food@mail.ru';
			$subject 	= "Новый заказ #".$res1;
			$headers ='Content-type: text/html; charset=UTF-8' . "\r\n".
				 'From: info@tortuga-food.com' . "\r\n" .
   				 'Reply-To: info@tortuga-food.com' . "\r\n" .
   				 'X-Mailer: PHP/' . phpversion();
			
			$message = "Получен новый заказ\n";
			$message .= "<h1>Заказ №".$res1."</h1><br/><br/>";
			$message .= "Заказчик: ".$name."<br/>";
			$message .= "Номер телефона: ".$phone."<br/>";
			$message .= "Адрес: ".$adress."<br/><br/>";
			$message .= $date."/ ".$time."<br/><br/>";
			$message .= "<a href=\"http://tortuga-food.com/admin/\">Перейти в Админ-Панель</a>\n";
			$message .= "<br/><br/>--";
			
			
			mail($to, $subject, $message, $headers);
			mail("info@tortuga-food.com", $subject, $message, $headers);
            
/*            if ($_GET["ajax"] == 1)
                echo "<script language='javascript'>null_cart();</script>";
            else */
            	null_cart();
      //  }
        
        return $res;
    }

    
    function add_to_cart($id){ //Функция добавляет элемент в корзину
        $_SESSION["cart"][$id] = $_SESSION["cart"][$id] + 1; 
    }
    
    function minus_from_cart($id){ //Функция отнимает 1 элемент корзины
        $_SESSION["cart"][$id] = $_SESSION["cart"][$id] - 1; 
    }
    
    function delete_from_cart($id){ //Функция удаляет элемент корзины
        $_SESSION["cart"][$id] = 0; 
    }
?>