            /* Administration functions */
            function show_orders(status){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=show_orders&status="+status,  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html); 
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
            function show_pages(){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=show_pages",  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html); 
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
				
            function update_pages(){  
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=show_pages",  
                cache: false, 
                success: function(html){  
                    $("#section").html(html); 
                    }
                });  
            } 		

			
            function edit_page(id){  
				var ob=document.getElementById('loading');
				var ob1=document.getElementById('page');
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=edit_page&id="+id,  
                cache: false, 
                beforeSend: function(html){  
					ob1.style.display='block';
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#page").html(html); 
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
            function save_page(f){  
					name= f.name.value;
					text= f.text.value;
					id	= f.id.value;
				$.post("php/admin_page.php?ajax=1&do=save_page", { "name": name, "text": text, "id": id }, function(data){
					update_pages();																									
 					alert(data);
				});	
					
				/*
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=save_page&id="+id+"&text="+text+"&name="+name,  
                cache: false, 
                beforeSend: function(html){  
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#page").html(html); 
					update_pages();
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
				*/
            } 
            
            
            function accept_order(id){  
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=accept_order&id="+id,  
                cache: false, 
				beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#stat").html(html);
					ob.style.display='block';
                    },
                complete: function(html){  
                    $("#accept_but").hide("fast");
                    show_orders(0);
					ob.style.display='none';
                    }
                });  
            } 
            
            function cancel_order(id){  
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=cancel_order&id="+id,  
                cache: false, 
				beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#stat").html(html);  
                    },
                complete: function(html){  
                    $("#accept_but").hide("fast");
                    show_orders(1);
					ob.style.display='none';
                    }
                });  
            } 
            
            function delete_order(id, status){ 
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=delete_order&id="+id,  
                cache: false, 
				beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#stat").html(html);  
                    },
                complete: function(html){  
                    $("#delete_but").hide("fast");
                    show_orders(status);
					ob.style.display='none';
                    }
                });  
            } 
            
            function restore_order(id){  
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=restore_order&id="+id,  
                cache: false,
				beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#stat").html(html);  
                    },
                complete: function(html){  
                    $("#accept_but").hide("fast");
                    show_orders(2);
					ob.style.display='none';
                    }
                });  
            } 
            
            function view_order(id){ 
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=view_order&id="+id,  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").show("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#page").html(html);  
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
            function view_statistic_files(){
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_statistic.php?ajax=1&do=view_files",  
                cache: false, 
                beforeSend: function(html){  
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#stat_files").html(html);  
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                }); 
            } 
			
            function view_statistic(file, mode){  
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_statistic.php?ajax=1&do=view_table&file="+file+"&mode="+mode,  
                cache: false, 
              	beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html);
					
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
