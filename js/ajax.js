  		function add_action_to_stat(action){
				action = action+" (JS)";
                $.ajax({  
                url: "php/sniffer.php?ajax=1&action="+action,  
                cache: false
                });  
            } 
			
  		function add_to_cart(id){  
				var ob=document.getElementById('add'+id);
				var ob1=document.getElementById('loading');
                $.ajax({  
                url: "php/cart.php?do=add_to_cart&ajax=1&id="+id,  
                cache: false, 
                beforeSend: function(html){  
                   ob1.style.display='block';
                    },
                success: function(html){  
                    add_action_to_stat("add_to_cart_"+id);
                    $("#cart").html(html);  
					 ob.style.display='block';
                    },
                complete: function(html){  
                    ob1.style.display='none';
                    }
                });  
            } 
    
             function show_cart_info(){  
			 	var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/cart.php?do=show_cart_info&ajax=1",  
                cache: false, 
                beforeSend: function(html){  
                   ob.style.display='block';
                    },
                success: function(html){  
                    $("#cart").html(html);
                    },
                complete: function(html){  
                    ob.style.display='none';
                    }
                });  
            }            

            
            function show_section(section){  
				var ob=document.getElementById('loading');
			
                $.ajax({  
                url: "php/show_section.php?ajax=1&section="+section,  
                cache: false, 
                beforeSend: function(html){  
                   ob.style.display='block';
				   show_cart_info();
                    },
                success: function(html){  
                    $("#content").html(html); 
                    add_action_to_stat("section_"+section);
                    },
                complete: function(html){  
                    ob.style.display='none';
                    }
                });  
            } 
            
            function minus_from_cart(id){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/cart.php?ajax=1&do=minus_from_cart&type=minus&id="+id,  
                cache: false, 
                beforeSend: function(html){  
                   ob.style.display='block';
                    },
                success: function(html){  
                    $("#cart").html(html);  
                    },
                complete: function(html){  
                    ob.style.display='none';
                    }
                });  
            } 
			
            function delete_from_cart(id){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/cart.php?ajax=1&do=delete_from_cart&type=minus&id="+id,  
                cache: false, 
                beforeSend: function(html){  
                   ob.style.display='block';
                    },
                success: function(html){  
                    $("#cart").html(html);  
                    },
                complete: function(html){  
                    ob.style.display='none';
                    }
                });  
            } 
            
            function plus_to_cart(id){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/cart.php?ajax=1&do=add_to_cart&type=plus&id="+id,  
                cache: false,
                beforeSend: function(html){  
                   ob.style.display='block';
                    },	
                success: function(html){  
                    $("#cart").html(html);  
                    },
                complete: function(html){  
                    ob.style.display='none';
                    }
                });  
            } 
            
            
            function show_cart(){  
                $.ajax({  
                url: "php/cart.php?ajax=1&do=show_cart",  
                cache: false, 
                beforeSend: function(html){  
                    $("#loading").show("fast");
                    $("#section").hide("fast");
                    },
                success: function(html){  
                    $("#section").html(html);  
                    },
                complete: function(html){  
                    $("#loading").hide("fast");
                    $("#section").show("fast");
                    }
                });  
            }

            
            function null_cart(){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/cart.php?do=null_cart&ajax=1",  
                cache: false, 
                beforeSend: function(html){  
                    ob.style.display='block';
                    },
                success: function(html){  
                    $("#cart").html(html);  
                    },
                complete: function(html){  
                 	 ob.style.display='none';
                    }
                });  
            } 
            
            function show_page(page){  
				var ob=document.getElementById('loading');
					action = "page_"+page;
				add_action_to_stat(action);
                $.ajax({  
                url: "php/show_page.php?ajax=1&page="+page,  
                cache: false, 
                beforeSend: function(html){  
                    ob.style.display='block';
                    },
                success: function(html){  
					
                    $("#content").html(html); 
                    },
                complete: function(html){  
                   ob.style.display='none';
                    }
                });  
            } 
            
            function order(){  
                $.ajax({  
                url: "php/cart.php?ajax=1&do=order",  
                cache: false, 
                beforeSend: function(html){  
                    $("#loading").show("fast");
                    $("#section").hide("fast");
                    },
                success: function(html){  
                    $("#section").html(html);  
                    },
                complete: function(html){
                    $("#loading").hide("fast");
                    $("#section").show("fast");
                    }
                });  
            } 
			
            function show_order_form(){  
			var ob=document.getElementById('loading');
				a=Math.random();

			
                $.ajax({  
                url: "php/order_form.php?ajax=1&cache="+a,  
                cache: false, 
                beforeSend: function(html){  
					ob.style.display='block';
					show_cart_info();
                    },
                success: function(html){  
                    $("#content").html(html);
                    add_action_to_stat("order_form");
                    },
                complete: function(html){
                    ob.style.display='none';
                    }
                });  
            } 
			
            function show_mail_form(){  
			var ob=document.getElementById('loading');
				a=Math.random();

			
                $.ajax({  
                url: "php/mail_form.php?ajax=1&cache="+a,  
                cache: false, 
                beforeSend: function(html){  
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#content").html(html);  
                    add_action_to_stat("mail_form");
                    },
                complete: function(html){
                    ob.style.display='none';
                    }
                });  
            } 
			
            function show_check_form(){  
			var ob=document.getElementById('loading');
				a=Math.random();

			
                $.ajax({  
                url: "php/check_form.php?ajax=1&cache="+a,  
                cache: false, 
                beforeSend: function(html){  
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#content").html(html);  
                    add_action_to_stat("check_form");
                    },
                complete: function(html){
                    ob.style.display='none';
                    }
                });  
            } 
			
            function check_status(f){
            var id=f.id.value;
                $.ajax({  
                url: "php/order.php?ajax=1&do=check_status&id="+id,  
                cache: false, 
                beforeSend: function(html){
                    $("#status").hide("fast");
                    },
                success: function(html){  
                    $("#status").html(html);  
                    },
                complete: function(html){
                    $("#status").show("fast");
                    }
                });  
                return false;
            } 
			
	         function do_order(f){
            var 
				ob			=	document.getElementById('loading');
             	name		=	f.name.value;
             	phone		=	f.phone.value;
             	street		=	f.street.value;
			 	house		=	f.house.value;
			 	flat			=	f.flat.value;
			 	keystring	=	f.keystring.value;
				scount		=	f.scount.value;
				other		=	f.other.value;
				
                if ((house == "") || (name == "") || (phone == "") || (keystring == "") || (f.street.selectedIndex == 0) || (f.keystring.value.length < 5)){
                    alert("Проверьте заполненность полей!");
                    return false;
                }
                else{
                    $.ajax({  
					type: "POST",
                    url: "php/order_form.php?do=do_order&ajax=1", 
					data: ({
						name: name,
						house: house,
						phone: phone,
						keystring: keystring,
						flat: flat,
						street: street,
						streetID: f.street.selectedIndex,
						scount: scount,
						other: other
					}),
                    cache: false, 
                    beforeSend: function(html){  
                        ob.style.display='block';
                    },
                    complete: function(html){
						show_cart_info();
                        
                    },
                    success: function(html){
                        $("#content").html(html);  
						ob.style.display='none';
                        add_action_to_stat("do_order");
                    }
                }); 
                }

            } 
			
	         function do_mail(f){
            var 
				ob			=	document.getElementById('loading');
             	name		=	f.name.value;
             	phone		=	f.phone.value;
             	email		=	f.email.value;
			 	keystring	=	f.keystring.value;
				message	=	f.message.value;
				
                if ((name == "") || (message == "") || (email == "") || (keystring == "") || (f.keystring.value.length < 5)){
                    alert("Проверьте заполненность полей!");
                    return false;
                }
                else{
                    $.ajax({  
					type: "POST",
                    url: "php/mail_form.php?ajax=1", 
					data: ({
						name: name,
						email: email,
						phone: phone,
						keystring: keystring,
						message: message
					}),
                    cache: false, 
                    beforeSend: function(html){  
                        ob.style.display='block';
                    },
                    success: function(html){
						$("#content").html(html); 
                        add_action_to_stat("do_mail"); 
						ob.style.display='none';
                    }
                }); 
                }

            } 
			
	         function do_check(f){
            var 
				ob			=	document.getElementById('loading');
             	number		=	f.number.value;
			 	keystring	=	f.keystring.value;
				
                if ((number == "") || (keystring == "") || (f.keystring.value.length < 5)){
                    alert("Проверьте заполненность полей!");
                    return false;
                } 
				else if (f.number.value.length < 5){
                    alert("Номер заказа введен неверно!");
                    return false;
                }
                else{
                    $.ajax({  
					type: "POST",
                    url: "php/check_form.php?ajax=1", 
					data: ({
						number: number,
						keystring: keystring
					}),
                    cache: false, 
                    beforeSend: function(html){  
                        ob.style.display='block';
                    },
                    success: function(html){
						$("#content").html(html);  
						ob.style.display='none';
                        add_action_to_stat("do_check");
                    }
                }); 
                }

            } 
	         function refresh_check(id){
            var 
				ob			=	document.getElementById('loading');
				
                    $.ajax({  
					type: "POST",
                    url: "php/check.php?ajax=1", 
					data: ({
						id: id
					}),
                    cache: false, 
                    beforeSend: function(html){  
                        ob.style.display='block';
                    },
                    success: function(html){
						$("#content").html(html);  
						ob.style.display='none';
                        add_action_to_stat("do_check_s");
                    }
                }); 

            } 
   /*         
            function do_order(f){
            var 
				ob			=	document.getElementById('loading');
             	name		=	f.name.value;
             	phone		=	f.phone.value;
             	street		=	f.street.value;
			 	house		=	f.house.value;
			 	flat			=	f.flat.value;
			 	keystring	=	f.keystring.value;
				scount		=	f.scount.value;
				other		=	f.other.value;
                if ((house == "") || (name == "") || (phone == "") || (keystring == "") || (f.street.selectedIndex == 0) || (f.keystring.value.length < 5)){
                    alert("Проверьте заполненность полей!");
                    return false;
                }
                else{
                    $.ajax({  
                    url: "php/order_form.php?do=do_order&ajax=1&name="+name+"&house="+house+"&phone="+phone+"&keystring="+keystring+"&flat="+flat+"&street="+street+"&streetID="+f.street.selectedIndex+"&scount="+scount+"&other="+other, 
                    cache: false, 
                    beforeSend: function(html){  
                        ob.style.display='block';
                    },
                    complete: function(html){
						show_cart_info();
                        
                    },
                    success: function(html){
                        $("#content").html(html);  
						ob.style.display='none';
                    }
                }); 
                }

            } 
*/


