//********* Функции для работы с заказами
            function show_orders(status){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=show_orders&status="+status,  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html); 
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
			function change_status(id, status_before, f){ 	
			var ob=document.getElementById('loading');
				status = f.selectedIndex;
				$.ajax({
                url: "php/admin_order.php?ajax=1&do=change_status&id="+id+"&from="+status_before+"&to="+status,  
                cache: false, 
				beforeSend: function(html){  
						ob.style.display='block';
                    },
                success: function(html){  
						show_orders(status_before);
                    },	
                complete: function(html){  
						ob.style.display='none';
                    }
				});
			}
			
            function view_order(id){ 
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_order.php?ajax=1&do=view_order&id="+id,  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").show("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#page").html(html);  
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
//********* Функции для работы со страницами			
            function show_pages(){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=show_pages",  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html); 
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
			
            function change_stick_page(id){  
            	var meth = document.getElementById('meth'+id);
            	methv = meth.value;
            	
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=change_stick&id="+id+"&meth="+methv,  
                cache: false, 
                success: function(html){  
                	meth.value = methv + " (сохранено)";
                    }
                });  
            } 
			
            function edit_page(id){  
				var ob=document.getElementById('loading');
				var ob1=document.getElementById('page');
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=edit_page&id="+id,  
                cache: false, 
                beforeSend: function(html){  
					ob1.style.display='block';
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#page").html(html); 
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
			
            function save_page(f){  
					name= f.name.value;
					text= f.text.value;
					id	= f.id.value;
				$.post("php/admin_page.php?ajax=1&do=save_page", { "name": name, "text": text, "id": id }, function(data){
					update_pages();																									
 					alert(data);
				});	
            } 
			
            function update_pages(){  
                $.ajax({  
                url: "php/admin_page.php?ajax=1&do=show_pages",  
                cache: false, 
                success: function(html){  
                    $("#section").html(html); 
                    }
                });  
            } 		
			
//********* Функции для работы с меню			
            function show_cats(){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_menu.php?ajax=1&do=show_cats",  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html); 
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            }

           function show_menu(section){  
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_menu.php?ajax=1&do=show_menu&section="+section,  
                cache: false, 
                beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html); 
                    
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            }

            function change_stick_menu(id, section){  
			var meth = document.getElementById('meth'+id);
				
				methv = meth.value;
                $.ajax({  
                url: "php/admin_menu.php?ajax=1&do=change_stick&id="+id+"&meth="+methv,  
                cache: false, 
                success: function(html){  
                    	meth.value = methv + " (сохранено)";
                    }
                });  
            } 
			
            function change_stick_section(id){  
			var meth = document.getElementById('meth'+id);
				
				methv = meth.value;
                $.ajax({  
                url: "php/admin_menu.php?ajax=1&do=change_stick_section&id="+id+"&meth="+methv,  
                cache: false, 
                success: function(html){  
                    	meth.value = methv + " (сохранено)";
                    }
                });  
            } 
			
//********* Функции для работы со статистикой			
            function view_statistic_files(){
				var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_statistic.php?ajax=1&do=view_files",  
                cache: false, 
                beforeSend: function(html){  
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#stat_files").html(html);  
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                }); 
            } 
			
            function view_statistic(file, mode){  
			var ob=document.getElementById('loading');
                $.ajax({  
                url: "php/admin_statistic.php?ajax=1&do=view_table&file="+file+"&mode="+mode,  
                cache: false, 
              	beforeSend: function(html){  
                    $("#page").hide("fast");
					ob.style.display='block';
                    },
                success: function(html){  
                    $("#section").html(html);
					
                    },
                complete: function(html){  
					ob.style.display='none';
                    }
                });  
            } 
            

            

    			
    		      
    		                               
    			 	
