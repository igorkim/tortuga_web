<?
	date_default_timezone_set('Asia/Vladivostok');
	function getExtension($filename) { //Определяет расширение файла
   		return end(explode(".", $filename));
  	}	
  	
	$dir 		= "../statistic/";
	$dest_dir	= "../statistic/archive/";

	if (is_dir($dir)) {
   		$files  = scandir($dir);
		$cnt    = count($files);
		$time	= time();
		$date 	= date("Y.m.d", $time);
		for ($i=0; $i<$cnt; $i++){
			if ((getExtension($files[$i]) == "csv") || (getExtension($files[$i]) == "txt")){
				if (!stristr($files[$i], $date)){
				
					$file 		= $dir.$files[$i];
					$dest_file	= $dest_dir.$files[$i];
					copy($file, $dest_file);
					unlink($file);
				}
			}
		}
	}	
?>
